# winweb

AWS cloudformation script to spin up a Windows server 2019 EC2 instance with HA and IIS postscript

## Exercise

This spins up an Ec2 instance and Install IIS Feature

### Parameters
- *Build File* : "*winweb-app* (for unmodified index.html) or *winweb-mod* (for modified index.html)"